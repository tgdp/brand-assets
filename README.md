# Brand assets

[![License: MIT-0](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/license/mit-0) [![CHAOSS DEI: Bronze](https://gitlab.com/tgdp/governance/-/raw/main/CHAOSS-Bronze-Badge-small.png?ref_type=heads)](https://badging.chaoss.community/)

The Brand Assets repository is a space for maintaining [The Good Docs Project](https://www.thegooddocsproject.dev/) branding guidelines and assets, such as the Doctopus.
